//---------------------------------------------------------------------------


#pragma hdrstop

#include "CsvReader.h"
#include <FileCtrl.hpp>

#include <fstream>

#include <string>

#include <sstream>

#include <stdio.h>
#include <vcl.h>
#include <DateUtils.hpp> 

//---------------------------------------------------------------------------

#pragma package(smart_init)


inline bool isInteger(const std::string & s)
{
   if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false ;

   char * p ;
   strtol(s.c_str(), &p, 10) ;

   return (*p == 0) ;
}

CsvReader::CsvReader(): mTimeFilterEnabled(false), mFilterEnabled(false)
{
    mCheckerPolygonCointainPoint = new CheckerPolygonCointainPoint();
}

CsvReader::~CsvReader()
{
    delete mCheckerPolygonCointainPoint;
}
void CsvReader::ListujKatalogi(std::string Path)
{
   TSearchRec sr;
   int result;
   
   result = FindFirst(AnsiString(Path.c_str()) + "*.*", faAnyFile, sr);

   while (result == 0)
   {
      if ((sr.Name != ".") && (sr.Name != "..") && ((sr.Attr & faDirectory) > 0))
      {
         ListujKatalogi(Path + std::string(sr.Name.c_str()) + "\\");
      }
      if ((sr.Name != ".") && (sr.Name != "..") && (!(sr.Attr & faDirectory) > 0))
      {
         std::string NazwaPliku = Path + std::string(sr.Name.c_str() );
         //NazwaPliku = NazwaPliku.Delete(1, Edit->Text.Length() + 1);
         if ( ExtractFileExt(NazwaPliku.c_str()) == ".csv" ) {
            if(NazwaPliku != "" )
               mCsvFileToRead.push_back(NazwaPliku.c_str());
         }

      }
      result = FindNext(sr);
   }
   FindClose(sr);
}

void CsvReader::FileToMemory(String lDirToCsv)
{
    std::ifstream  data(lDirToCsv.c_str());
    mRecordsFromFile.clear();
    std::string line;
    OneRecord lOneRecord;
    while(std::getline(data,line))
    {
        std::stringstream  lineStream(line);
        std::string        cell;
        int lCeilNumber =0;
        while(std::getline(lineStream,cell,','))
        {
            switch(lCeilNumber)
            {
                case 0: { lOneRecord.PLATFORM= cell;} break;
                case 1: { lOneRecord.ARGOS_ID= cell;} break;
                case 2: { lOneRecord.DATE= cell;} break;
                case 3: { lOneRecord.LATITUDE= cell;} break;
                case 4: { lOneRecord.LONGITUDE= cell.substr(1);} break; //
                case 5: { lOneRecord.PRES= cell;} break;
                case 6: { lOneRecord.TEMP = cell;} break;
                case 7: { lOneRecord.PSAL= cell;} break;

            }
            lCeilNumber++;
            // You have a cell!!!!
        }
        mRecordsFromFile.push_back(lOneRecord);
    }
    data.close();
}



void CsvReader::Analyze()
{
    mCsvFileToRead.clear();
    ListujKatalogi(std::string(GetCurrentDir().c_str()) +"\\in\\");

    std::string lPathToFileOkData = std::string(GetCurrentDir().c_str()) +"\\out\\OKdata.csv";
    std::string lPathToFileWrongData = std::string(GetCurrentDir().c_str()) + "\\out\\Wrongdata.csv";
    FILE *lFileNameOkData = fopen(lPathToFileOkData.c_str(), "w");
    FILE *lFileNameWrongData = fopen(lPathToFileWrongData.c_str(), "w");


    if (lFileNameOkData == NULL || lFileNameWrongData == NULL)
  {
     ShowMessage("Problem with open data for save resoult.");
    return;
  }

    for(int i = 0 ; i < mCsvFileToRead.size() ; i ++ )
    {
        mRecordsFromFile.clear();
        FileToMemory( mCsvFileToRead.at(i) );
        bool lOkData;
        for(int j=0;  j < mRecordsFromFile.size(); j++)
        {
             lOkData = Filter(mRecordsFromFile.at(j) );
             if(lOkData == true)
             {

                 fprintf(lFileNameOkData, "%s,%s,%s,%s,-%s,%s,%s,%s\n", mRecordsFromFile.at(j).PLATFORM.c_str() ,
                                                        mRecordsFromFile.at(j).ARGOS_ID.c_str(),
                                                        mRecordsFromFile.at(j).DATE.c_str(),
                                                        mRecordsFromFile.at(j).LATITUDE.c_str(),
                                                        mRecordsFromFile.at(j).LONGITUDE.c_str(),
                                                        mRecordsFromFile.at(j).PRES.c_str(),
                                                        mRecordsFromFile.at(j).TEMP.c_str(),
                                                        mRecordsFromFile.at(j).PSAL.c_str()
                                                        );

             }
             else
             {
                 fprintf(lFileNameWrongData, "%s,%s,%s,%s,-%s,%s,%s,%s\n", mRecordsFromFile.at(j).PLATFORM.c_str() ,
                                                        mRecordsFromFile.at(j).ARGOS_ID.c_str(),
                                                        mRecordsFromFile.at(j).DATE.c_str(),
                                                        mRecordsFromFile.at(j).LATITUDE.c_str(),
                                                        mRecordsFromFile.at(j).LONGITUDE.c_str(),
                                                        mRecordsFromFile.at(j).PRES.c_str(),
                                                        mRecordsFromFile.at(j).TEMP.c_str(),
                                                        mRecordsFromFile.at(j).PSAL.c_str()
                                                        );

             }
        }
    }

    std::string PLATFORM ; //0
    std::string ARGOS_ID;  //1
    std::string DATE;      //2
    std::string LATITUDE;   //3
    std::string LONGITUDE;  //4
    std::string PRES;       //5
    std::string TEMP;       //6
    std::string PSAL;       //7

    fclose(lFileNameOkData);
    fclose(lFileNameWrongData);

}




bool CsvReader::Filter(OneRecord pOneRecord)
{
//sprawdzenie czy pierwszxa kolumna to liczba
    if( isInteger(pOneRecord.PLATFORM) == false)
        return false;

    Coordinates lCoordinates = mCoordinates;

    if(mFilterEnabled == true)
    {
        mCheckerPolygonCointainPoint->p.x =  lCoordinates.getX ( atof(pOneRecord.LONGITUDE.c_str()) );
        mCheckerPolygonCointainPoint->p.y =  lCoordinates.getY (atof(pOneRecord.LATITUDE.c_str()) );
        mCheckerPolygonCointainPoint->r.y =  lCoordinates.getY (atof(pOneRecord.LATITUDE.c_str()) );

        if( mCheckerPolygonCointainPoint->oblicz() ==false)
           return false;
    }


    if (mTimeFilterEnabled == true )
    {
         TDateTime pTimeRecord = StrToDate(pOneRecord.DATE.substr(0,10).c_str()) ;   //YYYY-MM-DDTHH:MI:SSZ

         if (CompareDate(pTimeRecord , mTo ) == GreaterThanValue)
            return false;

         if (CompareDate(mFrom  , pTimeRecord )  == GreaterThanValue  )
            return false;

    }

    return true;
}

void CsvReader::setFilter(std::vector<std::pair<double,double> > pPairCorrdsVec)
{
    int max_x;

    for(int i =0; i < pPairCorrdsVec.size(); i++ )
    {
        mCheckerPolygonCointainPoint->polygon[i].x = pPairCorrdsVec.at(i).first;
        if (mCheckerPolygonCointainPoint->polygon[i].x > max_x)
            max_x=mCheckerPolygonCointainPoint->polygon[i].x;
        mCheckerPolygonCointainPoint->polygon[i].y  = pPairCorrdsVec.at(i).second;
        mCheckerPolygonCointainPoint->n++;
    }

    //Wyznaczanie wsp�lrzednych drugiego konca odcinka
    mCheckerPolygonCointainPoint->r.x=max_x+1;

}

