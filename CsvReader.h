//---------------------------------------------------------------------------

#ifndef CsvReaderH
#define CsvReaderH
#include <vector.h>
#include "CheckerPolygonCointainPoint.h"
#include "Coordinates.h"
#include <vcl.h>

//---------------------------------------------------------------------------


typedef struct oneRecord
{
    std::string PLATFORM ; //0
    std::string ARGOS_ID;  //1
    std::string DATE;      //2
    std::string LATITUDE;   //3
    std::string LONGITUDE;  //4
    std::string PRES;       //5
    std::string TEMP;       //6
    std::string PSAL;       //7
}OneRecord;

class CsvReader
{
    public:
        CsvReader();
        ~CsvReader();
        void ListujKatalogi(std::string Path);
        void FileToMemory(String lDirToCsv);
        void Analyze();

        std::vector<OneRecord> mRecordsFromFile;
        Coordinates mCoordinates;

        void setFiltterTime(bool pEnabled){mTimeFilterEnabled = pEnabled;};
        void setFiltterTime(TDateTime lFrom, TDateTime lTo){mFrom = lFrom; mTo = lTo;};

        void setFilter(bool pEnabled){mFilterEnabled = pEnabled;}
        void setFilter(std::vector<std::pair<double,double> > pPairCorrdsVec);

    private:
        std::vector<AnsiString> mCsvFileToRead;

        bool Filter(OneRecord pOneRecord);
        CheckerPolygonCointainPoint *mCheckerPolygonCointainPoint;

        TDateTime mFrom;
        TDateTime mTo;
        bool mTimeFilterEnabled;

        bool mFilterEnabled;
};
#endif
