object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Huricane Analyzer v. 1.0'
  ClientHeight = 689
  ClientWidth = 625
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ImageMap: TImage
    Left = 8
    Top = 8
    Width = 593
    Height = 312
    OnMouseMove = ImageMapMouseMove
    OnMouseUp = ImageMapMouseUp
  end
  object LabelLongLatiInfo: TLabel
    Left = 279
    Top = 556
    Width = 241
    Height = 83
    Caption = 'LabelLongLatiInfo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -31
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object buttDraw: TButton
    Left = 247
    Top = 386
    Width = 113
    Height = 25
    Caption = 'Refresh image'
    TabOrder = 0
    OnClick = buttDrawClick
  end
  object ButtonLoadMap: TButton
    Left = 247
    Top = 417
    Width = 113
    Height = 25
    Caption = 'Load Map'
    TabOrder = 1
    OnClick = ButtonLoadMapClick
  end
  object buttStartFilter: TButton
    Left = 247
    Top = 355
    Width = 113
    Height = 25
    Caption = 'Start filtration'
    TabOrder = 2
    OnClick = buttStartFilterClick
  end
  object P: TButton
    Left = 247
    Top = 479
    Width = 82
    Height = 25
    Caption = 'Draw Csv'
    TabOrder = 3
    OnClick = PClick
  end
  object GroupBoxTimeFilter: TGroupBox
    Left = 8
    Top = 590
    Width = 233
    Height = 95
    Caption = 'G'
    Enabled = False
    TabOrder = 4
    object Label1: TLabel
      Left = 16
      Top = 32
      Width = 31
      Height = 13
      Caption = 'From: '
    end
    object Label2: TLabel
      Left = 16
      Top = 72
      Width = 16
      Height = 13
      Caption = 'To:'
    end
    object DateTimePickerFrom: TDateTimePicker
      Left = 69
      Top = 28
      Width = 113
      Height = 21
      Date = 41202.733193159720000000
      Time = 41202.733193159720000000
      TabOrder = 0
    end
    object DateTimePickerTo: TDateTimePicker
      Left = 69
      Top = 68
      Width = 113
      Height = 21
      Date = 41213.733193159720000000
      Time = 41213.733193159720000000
      TabOrder = 1
    end
  end
  object GroupBoxCoordinates: TGroupBox
    Left = 8
    Top = 326
    Width = 233
    Height = 258
    Caption = 'GroupBoxCoordinates'
    TabOrder = 5
    object StringGridCoordinates: TStringGrid
      Left = 3
      Top = 73
      Width = 219
      Height = 152
      ColCount = 3
      RowCount = 1
      FixedRows = 0
      ParentColor = True
      TabOrder = 0
    end
    object editLong: TLabeledEdit
      Left = 68
      Top = 46
      Width = 65
      Height = 21
      EditLabel.Width = 47
      EditLabel.Height = 13
      EditLabel.Caption = 'Longitude'
      ParentColor = True
      TabOrder = 1
    end
    object editLati: TLabeledEdit
      Left = 135
      Top = 46
      Width = 57
      Height = 21
      EditLabel.Width = 39
      EditLabel.Height = 13
      EditLabel.Caption = 'Latitude'
      ParentColor = True
      TabOrder = 2
    end
    object buttInsertLongLati: TButton
      Left = 3
      Top = 42
      Width = 59
      Height = 31
      Caption = 'Insert'
      TabOrder = 3
      OnClick = buttInsertLongLatiClick
    end
    object buttClearLongLati: TButton
      Left = 172
      Top = 230
      Width = 50
      Height = 25
      Caption = 'Clear list'
      TabOrder = 4
      OnClick = buttClearLongLatiClick
    end
  end
  object CheckBoxCoordinatesFilter: TCheckBox
    Left = 8
    Top = 326
    Width = 122
    Height = 22
    BiDiMode = bdLeftToRight
    Caption = 'Coordinates filtter'
    ParentBiDiMode = False
    TabOrder = 6
    OnClick = CheckBoxCoordinatesFilterClick
  end
  object CheckBoxTimeFilterEnabled: TCheckBox
    Left = 8
    Top = 590
    Width = 122
    Height = 22
    BiDiMode = bdLeftToRight
    Caption = 'Time filter enabled'
    ParentBiDiMode = False
    TabOrder = 7
    OnClick = CheckBoxTimeFilterEnabledClick
  end
  object PanelColorPoints: TPanel
    Left = 327
    Top = 479
    Width = 33
    Height = 30
    Color = clBlack
    ParentBackground = False
    TabOrder = 8
    OnClick = PanelColorPointsClick
  end
  object ButtonSaveImage: TButton
    Left = 247
    Top = 448
    Width = 113
    Height = 25
    Caption = 'Save image'
    TabOrder = 9
    OnClick = ButtonSaveImageClick
  end
  object OpenDialog1: TOpenDialog
    Left = 496
    Top = 416
  end
  object ColorDialog1: TColorDialog
    Left = 512
    Top = 456
  end
  object MainMenu1: TMainMenu
    Left = 312
    Top = 16
    object Info1: TMenuItem
      Caption = 'Load Map'
      OnClick = Info1Click
    end
    object Info2: TMenuItem
      Caption = 'Info'
      OnClick = Info2Click
    end
  end
end
