//---------------------------------------------------------------------------

#ifndef CheckerPolygonCointainPointH
#define CheckerPolygonCointainPointH

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

//---------------------------------------------------------------------------

class CheckerPolygonCointainPoint
{
public:

    CheckerPolygonCointainPoint():n(0)
    {
    }
    bool oblicz();

    struct vertex 			//Wierzcholek
    {
        int 		x,y;
    };
    vertex 	polygon[20];	//Tablica wierzcholow wielokata, max: 20
    vertex 	p;				//Dany punkt p
    vertex 	r;				//Drugi koniec odcinka |PR|
    vertex 	tmp;
    int 		n;			//Liczba wierzchalkow wielokata
    int 		k;

private:

   int min(int a, int b);
   int max(int a, int b);
   int sign(int a);
   int przynaleznosc(vertex x, vertex y,vertex z);
   int det(vertex x,vertex y,vertex z);
   int przecinanie(vertex a,vertex b);




};

#endif
