//---------------------------------------------------------------------------

#ifndef CoordinatesH
#define CoordinatesH
//---------------------------------------------------------------------------

class Coordinates
{
public:
    double getX(double pLongitude)
    {
        return (mRefX2 - mRefX1) - (mRefX2 - mRefX1)*(pLongitude -mRefLong2 ) / ( (mRefLong1 - mRefLong2) ) ;
    };
    double getY(double pLatitude){return  mRefY2 -  (mRefY2 - mRefY1)*   (pLatitude) / ( (mRefLati1 - mRefLati2) )   ;}
//        double getLatitude(){return mLatitude;}
//        double getLongitude(){return mLongitude;}

    double getLatitude(double pY){ return mRefLati1 - (mRefLati1 - mRefLati2)* pY / (mRefY2 - mRefY1); }
    double getLongitude(double pX){return mRefLong1 - (mRefLong1 - mRefLong2)* pX / (mRefX2 - mRefX1); }

    void setRefRect(double pX1, double pX2, double pY1, double pY2)
    {
        mRefX1 = pX1;
        mRefX2 = pX2;
        mRefY1 = pY1;
        mRefY2 = pY2;
    }

     void setRefCoord(double pLongitude1, double pLongitude2 ,double pLatitude1, double pLatitude2 )
    {
        mRefLati1 = pLatitude1;
        mRefLati2 = pLatitude2;
        mRefLong1 =  pLongitude1;
        mRefLong2 =  pLongitude2;
    }

    private:
        //ref
        double mRefX1;
        double mRefX2;
        double mRefY1;
        double mRefY2;

        double mRefLati1;
        double mRefLati2;
        double mRefLong1;
        double mRefLong2;

};

#endif
