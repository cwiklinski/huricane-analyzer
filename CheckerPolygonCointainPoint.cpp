//---------------------------------------------------------------------------


#pragma hdrstop

#include "CheckerPolygonCointainPoint.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//funkcja min
int CheckerPolygonCointainPoint::min(int a, int b)
{
   if (a<b) return a;
   return b;
}
//funkcja max
int CheckerPolygonCointainPoint::max(int a, int b)
{
   if (a>b) return a;
   return b;
}
//funkcja signum
int CheckerPolygonCointainPoint::sign(int a)
{
   if (a == 0)
      return 0;
   if (a < 0)
      return -1;
   return 1;
}


//********************************************************
int CheckerPolygonCointainPoint::przynaleznosc(vertex x, vertex y,vertex z)
{
    int	det;
    det=x.x*y.y+y.x*z.y+z.x*x.y-z.x*y.y-x.x*z.y-y.x*x.y;
    if (det!=0) return(0); else
    {
        if ((min(x.x,y.x)<=z.x)&&(z.x<=max(x.x,y.x)) && (min(x.y,y.y)<=z.y)&&(z.y<=max(x.y,y.y)))
            return(1);
        else
            return(0);
    };
};
//********************************************************
int CheckerPolygonCointainPoint::det(vertex x,vertex y,vertex z) //Wyznacznik macierzy kwadratowej stopnia 3.
{
    return(x.x*y.y+y.x*z.y+z.x*x.y-z.x*y.y-x.x*z.y-y.x*x.y);
}
//********************************************************
int CheckerPolygonCointainPoint::przecinanie(vertex a,vertex b)
{
    if ((przynaleznosc(p,r,a)==0)&&(przynaleznosc(p,r,b)==0))
    { //p�lprosta nie przecina odcinka |AB| w koncach
        if ((sign(det(p,r,a)) != sign(det(p,r,b))) &&
            (sign(det(a,b,p)) != sign(det(a,b,r))))
            return(1);
        else
            return(0);
    } else //do p�lprostej nalezy przynajmniej jeden koniec odcinka |AB|
    {
    if ((przynaleznosc(p,r,a)==1)&&(przynaleznosc(p,r,b)==1))
    {
      if ((sign(det(p,r,polygon[(k-1+n)%n])) == sign(det(p,r,polygon[(k+2)%n]))) &&
          (sign(det(p,r,polygon[(k-1+n)%n])) != 0)) return(0); else return(1);
      } else
      if ((przynaleznosc(p,r,polygon[(k-1+n)%n])==1)||(przynaleznosc(p,r,polygon[(k+2)%n])==1)) return(0);
        else
        { //polprosta zawiera tylko wierzcholek
            if (przynaleznosc(p,r,b)==1)
                {
                  tmp=a;
                  return(0);
                }
                if (przynaleznosc(p,r,a)==1)
                {
                  if ((sign(det(p,r,tmp)) == sign(det(p,r,b))) &&
                      (sign(det(p,r,tmp)) != 0)) return(0); else return(1);
                }
         }
    }
    return 0;
}
//********************************************************
bool CheckerPolygonCointainPoint::oblicz()
{
    int       l=0; //liczba przeciec
    int       i;
    div_t		 w;
    //
    for (i=0;i<n;i++)
    {
       k=i;
       if (przynaleznosc(polygon[i], polygon[(i+1)%n], p) == 1)
       {
//          printf("Punkt nalezy do krawedzi wielokata\n");

          return true;
       }
       if (przecinanie(polygon[i],polygon[(i+1)%n])==1)
            l++;
    }
//    printf("Rozwiazanie--------------\n");
//    printf("Liczba przeciec: %i\n",l);
    w=div(l,2);
    if (w.rem==0)
    {
//        printf("Punkt p NIE nalezy do wielokata\n");
        return false;
    }
    else
    {
//        printf("Punkt p nalezy do wielokata\n");
        return true;
    }
}
//********************************************************
