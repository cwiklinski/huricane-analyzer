//---------------------------------------------------------------------------

#include <vcl.h>
#include <Jpeg.hpp>
#pragma hdrstop
#include <iostream>


#include "HurricaneAnalyzer.h"
#include <string>     // std::string, std::stod
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
    : TForm(Owner), mCsvReader(NULL)
{
  //obrazek
    try{
    TJPEGImage *jpg = new TJPEGImage();
    jpg->LoadFromFile(GetCurrentDir() +"//map1.jpg");  //wczytanie pliku w formacie jpg.

    mBmp = new Graphics::TBitmap();

    mBmp->Width = jpg->Width;
    mBmp->Height = jpg->Height;

    mBmp->Canvas->Draw(0, 0, jpg);  //przerysowanie grafiki w formacie jpg do obiektu bmp.

    ImageMap ->Width = jpg->Width;
    ImageMap->Height = jpg->Height;
    ImageMap->Picture->Bitmap = mBmp;

    jpg->Free();  // usuni�cie obiektu jpg z pami�ci.
    }catch(...)
    {
        ShowMessage(GetCurrentDir() +"//map1.jpg - brak pliku" );
    }

    StringGridCoordinates->Cells[0][0]= "Lp";
    StringGridCoordinates->Cells[1][0]= "Longitude";
    StringGridCoordinates->Cells[2][0]= "Latitude";

    mCsvReader = new CsvReader();

}
//---------------------------------------------------------------------------
void __fastcall TForm3::buttInsertLongLatiClick(TObject *Sender)
{
    StringGridCoordinates->RowCount++;

    int lRowToInsert = StringGridCoordinates->RowCount-1;
    StringGridCoordinates->Cells[0][lRowToInsert] = IntToStr( StringGridCoordinates->RowCount -1 );
    StringGridCoordinates->Cells[1][lRowToInsert] = editLong->Text;
    StringGridCoordinates->Cells[2][lRowToInsert] =  editLati->Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm3::buttDrawClick(TObject *Sender)
{
    ImageMap->Picture->Bitmap = mBmp;
    Coordinates lCoordinates;
    lCoordinates.setRefRect(0 ,ImageMap ->Width  ,0, ImageMap ->Height );
    lCoordinates.setRefCoord(110, 10, 50, 0 );

    int lSizePoligon = StringGridCoordinates->RowCount;

     TPoint *Pt = new TPoint[lSizePoligon];

    for(int i = 1 ;i <  StringGridCoordinates->RowCount; i++)
    {
         Pt[i-1] = Point(
                        lCoordinates.getX( StringGridCoordinates->Cells[1][i].ToDouble() ),
                        lCoordinates.getY( StringGridCoordinates->Cells[2][i].ToDouble() )
                        );
    }
    Pt[lSizePoligon -1 ] = Pt[0];
    ImageMap->Canvas->Polyline(Pt, StringGridCoordinates->RowCount-1);

    delete[] Pt;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::ButtonLoadMapClick(TObject *Sender)
{
    if(OpenDialog1->Execute())
    {
        TJPEGImage *jpg = new TJPEGImage();
        jpg->LoadFromFile(OpenDialog1->FileName);  //wczytanie pliku w formacie jpg.

        Graphics::TBitmap * bmp = new Graphics::TBitmap();

        bmp->Width = jpg->Width;
        bmp->Height = jpg->Height;

        bmp->Canvas->Draw(0, 0, jpg);  //przerysowanie grafiki w formacie jpg do obiektu bmp.

        ImageMap ->Width = jpg->Width;
        ImageMap->Height = jpg->Height;
        ImageMap->Picture->Bitmap = bmp;
        bmp->Free();  // usuni�cie obiektu bmp z pami�ci.
        jpg->Free();  // usuni�cie obiektu jpg z pami�ci.
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::ImageMapMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    StringGridCoordinates->RowCount++;
    Coordinates lCoordinates;

    lCoordinates.setRefRect(0 ,ImageMap ->Width  ,0, ImageMap ->Height );
    lCoordinates.setRefCoord(110, 10, 50, 0 );

    int lRowToInsert = StringGridCoordinates->RowCount-1;
    StringGridCoordinates->Cells[0][lRowToInsert] = IntToStr( StringGridCoordinates->RowCount -1 );
    StringGridCoordinates->Cells[1][lRowToInsert] = lCoordinates.getLongitude(X);
    StringGridCoordinates->Cells[2][lRowToInsert] =  lCoordinates.getLatitude(Y);

    buttDrawClick(Sender);
    if(mCsvReader != NULL)
        DrawCsvPoints();
}
//---------------------------------------------------------------------------
void __fastcall TForm3::buttClearLongLatiClick(TObject *Sender)
{
    StringGridCoordinates->RowCount = 1;
    buttDrawClick(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TForm3::FormDestroy(TObject *Sender)
{
     mBmp->Free();  // usuni�cie obiektu bmp z pami�ci.
}
//---------------------------------------------------------------------------
void __fastcall TForm3::buttStartFilterClick(TObject *Sender)
{

    Coordinates lCoordinates;
    lCoordinates.setRefRect(0 ,ImageMap ->Width  ,0, ImageMap ->Height );
    lCoordinates.setRefCoord(110, 10, 50, 0 );

    //czytanie csv


    mCsvReader->mCoordinates= lCoordinates;
    std::vector<std::pair<double,double> > lPairCorrdsVec;
    std::pair<double,double>  lToPush;
    for(int i = 1 ;i <  StringGridCoordinates->RowCount; i++)
    {
         lToPush.first =  lCoordinates.getX( StringGridCoordinates->Cells[1][i].ToDouble() );
         lToPush.second = lCoordinates.getY( StringGridCoordinates->Cells[2][i].ToDouble() );

        lPairCorrdsVec.push_back(lToPush);
    }


    //add time filtter
    if(CheckBoxTimeFilterEnabled->Checked)
    {
//    TDateTimePicker
        TDateTime lFrom = DateTimePickerFrom->DateTime.FormatString("yyyy-mm-dd");
        TDateTime lTo  =  DateTimePickerTo->DateTime.FormatString("yyyy-mm-dd");
        mCsvReader->setFiltterTime(true);

        mCsvReader->setFiltterTime(lFrom, lTo);
    }else{
        mCsvReader->setFiltterTime(false);

    }
    //end time filtter

    if( CheckBoxCoordinatesFilter->Checked )
    {
        mCsvReader->setFilter(true);
        mCsvReader->setFilter(lPairCorrdsVec);
    }else{

        mCsvReader->setFilter(false);
    }


    mCsvReader->Analyze();
//    delete mCsvReader;
}
//---------------------------------------------------------------------------
void __fastcall TForm3::PClick(TObject *Sender)
{
    if(OpenDialog1->Execute() == false)
        return;

//    mCsvReader = new CsvReader();
    mCsvReader->FileToMemory(OpenDialog1->FileName);
    DrawCsvPoints();

//    delete mCsvReader;
}
//---------------------------------------------------------------------------

void TForm3::DrawCsvPoints()
{
    Coordinates lCoordinates;
    lCoordinates.setRefRect(0 ,ImageMap ->Width  ,0, ImageMap ->Height );
    lCoordinates.setRefCoord(110, 10, 50, 0 );

    std::string::size_type sz;

    ImageMap->Canvas->Brush->Color = PanelColorPoints->Color ;
    for(int i =0; i < mCsvReader->mRecordsFromFile.size(); i++)
    {
        int x1= lCoordinates.getX( atof (mCsvReader->mRecordsFromFile.at(i).LONGITUDE.c_str() ) );
        int y1 =lCoordinates.getY( atof (mCsvReader->mRecordsFromFile.at(i).LATITUDE.c_str() ) );

        ImageMap->Canvas->Rectangle  (x1, y1, x1+15, y1+15);
    }
}
void __fastcall TForm3::CheckBoxTimeFilterEnabledClick(TObject *Sender)
{
    GroupBoxTimeFilter->Enabled = ((TCheckBox*)Sender)->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::CheckBoxCoordinatesFilterClick(TObject *Sender)
{
        GroupBoxCoordinates->Enabled = ((TCheckBox*)Sender)->Checked;    
}
//---------------------------------------------------------------------------

void __fastcall TForm3::PanelColorPointsClick(TObject *Sender)
{
//std::cout <<"super";
////TPrinterSetupDialog
//    TPrinter *a = new TPrinter() ;
//    a->
////TPreviewForm *Preview = new TPreviewForm( this );
//Preview->Execute( &PrintRoutine );
//delete Preview;
//   PrinterSetupDialog1->Execute ();
    if (ColorDialog1->Execute())
    {
        PanelColorPoints->Color =ColorDialog1->Color ;
    }
    
}
//---------------------------------------------------------------------------

void __fastcall TForm3::ButtonSaveImageClick(TObject *Sender)
{
    TJPEGImage *JI = new TJPEGImage();
    try
    {
        if(OpenDialog1->Execute() == false)
            return;

        JI->Assign(ImageMap->Picture->Bitmap);
        JI->SaveToFile(OpenDialog1->FileName);
    }
    __finally
    {
        delete JI;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm3::ImageMapMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
    Coordinates lCoordinates;
    lCoordinates.setRefRect(0 ,ImageMap ->Width  ,0, ImageMap ->Height );
    lCoordinates.setRefCoord(110, 10, 50, 0 );


    LabelLongLatiInfo->Caption = "X: " + AnsiString(lCoordinates.getLongitude(X)) + "\n" +
                                 "Y: " + AnsiString(lCoordinates.getLatitude(Y));
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Info2Click(TObject *Sender)
{
    ShowMessage("Program created by Beata and Wojciech �wikli�ski \n Wtirtten with use: Borland� C++Builder� 2005 ");
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Info1Click(TObject *Sender)
{
    ButtonLoadMapClick(Sender);
}
//---------------------------------------------------------------------------

